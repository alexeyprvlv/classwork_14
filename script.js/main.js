
function func () {
    let counter = 0;

    return function () {
        counter++;
        console.log(counter);
    }
};
let an = func();
an();

